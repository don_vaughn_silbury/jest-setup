import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { My1componentComponent } from './my1component.component';

describe('My1componentComponent', () => {
  let component: My1componentComponent;
  let fixture: ComponentFixture<My1componentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ My1componentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(My1componentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have a test', () => {
    fixture.detectChanges();
    const testElement = fixture.debugElement.nativeElement.querySelector('.test');
    expect(testElement.textContent.trim()).toBe('1');
  });
});
